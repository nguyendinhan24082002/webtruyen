export const MODAL = {
  LOGIN: 'modalLogin',
  REGISTER: 'modalRegister',
  ALERT_AUTHEN: 'modalAlertAuthentication',
  ALERT_AUTHEN_READING: 'modalAlertAuthenticationReading',
  SAVE__BOOKMARK: 'modalSaveBookmark',
  DELETE__BOOKMARK: 'modalDeleteBookmark',
  CHANGLE_PASSWORD: 'modalChanglePassword',
  FORGOT_PASSWORD: 'modalForgotPassword',
  ACCOUNT: 'modalAccount',
  AVATAR: 'modalAvatar',
  NAVIGATION: 'modalNavigation',
  HISTORY_TRANSACTION: 'modalHistoryTransaction',
  HISTORY_BETTING: 'modalHistoryBetting',
  HISTORY_SPORTS: 'modalHistorySports',
  TRANSFER_WALLET: 'modalTransferWallet',
  WELCOME: 'modalWelcome',
  PROMOTION_PACKAGE: 'modalPackage',
  NOTIFICATION: 'modalNotification'
}
export const REGEX_EMAIL = /(?=.*^[0-9a-zA-Z@.]+$)(?=.*^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$).*/

export const SHORT_DATE_FORMAT = 'DD-MM-YYYY'
export const DATE_FORMAT = 'DD/MM/YYYY'
export const FULL_DATE_FORMAT = 'DD-MM-YYYY HH:mm:ss'

export const DATETIME_FORMAT = {
  MAINTENANCE_FORMAT: 'HH:mm - DD/MM/YYYY'
}
export const ICON_ERROR = {
  warning: 'warning',
  error: 'error',
  success: 'success',
  info: 'info',
  connect: require('@/assets/img/alerts/connect.svg'),
  money: require('@/assets/img/alerts/money.svg'),
  promotion: require('@/assets/img/alerts/promotion.svg'),
  notify: require('@/assets/img/alerts/notify.svg'),
  casino: require('@/assets/img/alerts/casino.svg'),
  dollar: require('@/assets/img/alerts/dollar.svg')
}
