import globalMixin from '@/mixins/global-mixin'
import modalMixin from '@/mixins/modal'
import USER_API from '@/api/user'
import {
  MODAL
} from '@/config/constant'
import {
  // eslint-disable-next-line no-unused-vars
  BIconInfoSquareFill
} from 'bootstrap-vue'

export default {
  mixins: [globalMixin, modalMixin],
  methods: {
    async $_loginMixin_login (lpToken = null, redirect = null) {
      this.$_globalMixin_loading()
      try {
        const response = await this.$axios.$post(USER_API.LOG_IN, {
          ...this.dataLogin
        })
        const dataUserCurrent = response
        this.$store.dispatch('member/setCurenter', dataUserCurrent)
        this.$_loginMixin_reset()
        this.$_modalMixin_closeModal(MODAL.LOGIN)
      } catch (error) {
        this.$showAlert({
          error,
          icon: 'warning',
          title: 'LỖI',
          message: 'Đăng nhập thất bại ! Vui lòng kiểm tra lại thông tin.'
        })
        this.$_globalMixin_load()
      }
    },
    $_loginMixin_reset () {
      this.dataLogin = {
        email: '',
        password: ''
      }
    }
  }

}
