import globalMixin from '@/mixins/global-mixin'
import modalMixin from '@/mixins/modal'
import USER_API from '@/api/user'
import {
  MODAL
} from '@/config/constant'
export default {
  mixins: [globalMixin, modalMixin],
  methods: {
    async $_registerMixin_register () {
      this.$_globalMixin_loading()
      try {
        const response = await this.$axios.$post(USER_API.REGISTER, {
          ...this.dataRegister
        })
        if (response.status === undefined) {
          this.$_modalMixin_closeModal(MODAL.REGISTER)
          this.$showAlert({
            status: true,
            title: 'CHÚC MỪNG !',
            message: 'Bạn đã đăng ký thành viên thành công !',
            icon: 'success',
            button: 'Đóng',
            url: '/'
          })
        } else {
          this.$showAlert({
            message: 'Đã xảy ra lỗi vui lòng kiểm tra lại thông tin !',
            // eslint-disable-next-line no-undef
            error
          })
        }
        this.$_globalMixin_load()
      } catch (error) {
        this.$showAlert({
          message: 'Email đã được đăng ký hoặc xảy ra lỗi vui lòng kiểm tra lại !',
          error
        })
        this.$_globalMixin_load()
      }
    }
  }
}
