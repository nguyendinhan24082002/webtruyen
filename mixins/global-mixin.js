import STORYAPI from '@/api/story'

export default {
  data () {
    return {

      isLoading: false,
      categoryList: []
    }
  },
  computed: {
    isLogged () {
      return this.$store.getters['member/isLogged']
    }
  },
  methods: {
    $_globalMixin_loading () {
      this.isLoading = true
    },
    $_globalMixin_load () {
      this.isLoading = false
    },
    async fectCategory () {
      try {
        this.isLoading = true
        const listcategoryData = await this.$axios.$get(
          STORYAPI.STORY_LIST_CATEGORY
        )
        if (listcategoryData) {
          this.categoryList = listcategoryData.data
        }
      } catch (e) {
      }
    }
  },
  mounted () {
    this.fectCategory()
  }
}
