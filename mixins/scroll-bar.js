import $ from 'jquery'

export default {
  methods: {
    handleScroll () {
      const scrollTop = window.pageYOffset || document.documentElement.scrollTop
      if (scrollTop === 0) {
        $('.page').removeClass('page--scroll')
      } else {
        $('.page').addClass('page--scroll')
      }

      // Show/Hide scroll to top button
      if (scrollTop > 500) {
        $('.scroll-top').addClass('show-scroll-btn')
      } else {
        $('.scroll-top').removeClass('show-scroll-btn')
      }
    }
  },
  mounted () {
    const self = this
    window.addEventListener('scroll', function () {
      self.handleScroll()
    })
  }
}
