const CHAPTER_API = {

  GET_CHAPTER_BY_SLUG: 'get_chapter/',
  GET_DETAIL_STORY_BY_SLUG: 'get_story/',
  GET_LIST_CHAPTER_BY_STORY_ID: 'get_list_chapter_by_storyid/'
}

module.exports = CHAPTER_API
