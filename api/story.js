const STORY_API = {
  LIST_STORY_HOME_FIRST_LIST: 'home/get-first-list',
  STORY_HOME_TOP_LIST: 'home/get-top-list',
  STORY_HOME_STORE_LIST: 'home/get-store_category',
  STORY_LIST_CATEGORY: 'get_category',

  DETAIL_STORY_BY_SLUG: 'get_story/',

  LIST_STORY_BEST_READ: 'get_many_read',

  LIST_STORY_BY_SLUG_CATEGORY: 'list-truyen-by-theloai/',

  DETAIL_CATEGORY_BY_SLUG: 'detail-theloai/',

  SEARCHING_ADVANCE: 'home/filter-search',

  ADD_SAVED_STORY: 'add-saved-story',

  CHECK_SAVED_STORY: 'check-saved-story',

  DELETE_SAVED_STORY: 'delete-saved-story'
}

module.exports = STORY_API
