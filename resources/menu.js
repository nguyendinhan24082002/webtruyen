export const menus = [
  {
    title: 'Trang chủ',
    link: '/',
    name: 'trang-chu',
    loginRequired: false,
    newTab: false,
    mainWallet: true
  },
  {
    title: 'Thể loại',
    link: '/the-loai',
    name: 'category',
    loginRequired: true,
    newTab: false,
    mainWallet: true
  },
  {
    title: 'Tìm kiếm',
    link: '/tim-kiem',
    name: 'tim-kiem',
    loginRequired: true,
    newTab: false,
    mainWallet: true
  }
]
