import axios from 'axios'

import Vuex from 'vuex'
import STORYAPI from '@/api/story'

const createStore = () => {
  return new Vuex.Store({
    state: {
      decks: []
    },
    mutations: {
      setDecks (state, decks) {
        state.decks = decks
      }
    },
    actions: {
      nuxtServerInit (vuexContext, context) {
        return axios.get(STORYAPI).then((res) => {
          const deckArray = []
          for (const key in res.data) {
            deckArray.push({ ...res.data[key], id: key })
          }
          vuexContext.commit('setDecks', deckArray)
        })
          .catch((e) => {
            vuexContext.error(e)
          })
      },
      setDecks (vuexContext, decks) {
        vuexContext.commit = decks
      }
    }
  })
}

export default createStore()
