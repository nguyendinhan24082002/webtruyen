import state from './state'
import actions from './actions'
import mutations from './mutations'
import getters from './getters'

// eslint-disable-next-line no-unused-vars
const url = process.env.BASE_URL_API + 'story.json'
export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
