import axios from 'axios'

const url = process.env.BASE_URL_API + 'story.json'
export default {
  async nuxtServerInit (vuexContext) {
    await axios
      .get(url)
      .then((res) => {
        const storysArr = []
        for (const key in res.data) {
          storysArr.push({
            ...res.data[key],
            id: key
          })
        }
        // vuexContext.commit("SET_STORY", storysArr);
      })
      .catch((error) => {
        console.log(error)
      })
  },
  async loadStory (vuexContext) {
    await axios
      .get(url)
      .then((res) => {
        const storysArr = []
        for (const key in res.data) {
          storysArr.push({
            ...res.data[key],
            id: key
          })
        }
        vuexContext.commit('SET_STORY', storysArr)
      })
      .catch((error) => {
        console.log(error)
      })
  },
  addStory (vuexContext, storyData) {
    return axios
      .post(url, storyData + vuexContext.state.A.token)
      .then((data) => {})
      // eslint-disable-next-line node/handle-callback-err
      .catch((error) => {
        // eslint-disable-next-line no-undef
        console.log(e)
      })
  }
}
