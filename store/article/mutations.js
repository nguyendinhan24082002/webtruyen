export default {
  SET_ARTICLE (state, articles) {
    state.loadArticles = articles
  },
  SET_NUMPAGE (state, num_page) {
    state.num_page = num_page
  },
  SET_SHOW_PAGINATE (state, payload) {
    state.is_show_paginate = payload
  }
}
