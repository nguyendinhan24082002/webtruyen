export default {
  getArticle (state) {
    return state.loadArticles
  },
  getNumPage (state) {
    return state.num_page
  },
  getShowPaginate (state) {
    return state.is_show_paginate
  }
}
