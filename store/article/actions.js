export default {
  // async nuxtServerInit(vuexContext) {
  //   console.log("HELLO");
  //   // vuexContext.dispatch("loadArticles");
  // },
  async loadArticles (vuexContext, num_page) {
    const res = await this.$repositories.article.all(num_page)
    const { status, data } = res
    // console.log(data.data.module1);
    const module1 = data.data.module1
    if (status === 200) {
      vuexContext.commit('SET_ARTICLE', module1.data)
      const current_page = module1.current_page
      // console.log("current_page", current_page);

      const last_page = module1.last_page
      // console.log("last_page", last_page);
      if (current_page < last_page) {
        vuexContext.dispatch('setNumPage', current_page + 1)
        vuexContext.dispatch('setShowPaginate', true)
      } else {
        vuexContext.dispatch('setNumPage', current_page - 1)
        vuexContext.dispatch('setShowPaginate', false)
      }
    }
  },
  setNumPage (vuexContext, payload) {
    vuexContext.commit('SET_NUMPAGE', payload)
    vuexContext.commit('SET_SHOW_PAGINATE', payload)
  },
  setShowPaginate (vuexContext, payload) {
    vuexContext.commit('SET_SHOW_PAGINATE', payload)
  }
}
