import Vuex from 'vuex'
import storyModule from './story'
import memberModule from './member'
import articleModule from './article'

const createStore = () => {
  return new Vuex.Store({
    namespaced: true,
    // actions: {
    //   // eslint-disable-next-line require-await
    //   async nuxtServerInit (
    //     context, {
    //       app,
    //       req
    //     }) {
    //     const currentUser = app.$cookies.get('currentUser', {
    //       parseJSON: true
    //     })
    //     if (currentUser) {
    //       if (currentUser.access_token) {
    //         context.dispatch('member/setCurenter', currentUser)
    //       } else {
    //         context.dispatch('member/setCurenter', null)
    //       }
    //     } else {
    //       context.dispatch('member/setCurenter', null)
    //     }
    //   }
    // },
    modules: {
      story: storyModule,
      member: memberModule,
      article: articleModule
    }
  })
}

export default createStore
