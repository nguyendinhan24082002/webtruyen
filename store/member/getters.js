export default {
  getAuthen (state) {
    return state.isAuthen
  },
  getToken (state) {
    return state.loaderToken
  },
  getUser (state) {
    return state.loaderUser
  },
  getCurrentuser (state) {
    return state.currentUser
  },
  isLogged (state) {
    return !!state.currentUser
  }
}
