import axios from 'axios'

export default {

  setCurenter (vuexContext, payload) {
    if (payload) {
      vuexContext.commit('SET_CURRENT_USER', payload)
      this.$cookies.set('currentUser', payload)
    } else {
      vuexContext.commit('SET_CURRENT_USER', null)
      this.$cookies.set('currentUser', payload)
    }
  },
  // eslint-disable-next-line require-await
  async setUser ({
    commit
  }, payload) {
    commit('SET_USER', payload)
  },
  // eslint-disable-next-line require-await
  async setIsAuthen ({
    commit
  }, payload) {
    commit('SET_ISAUTHEN', payload)
  },
  // eslint-disable-next-line require-await
  async getToken ({
    commit
  }, payload) {
    commit('GET_TOKEN', payload)
  },
  async register (vuexContext, credentials) {
    const urlRegister =
      process.env.FIREBASE_AUTH_REGISTER_API + process.env.API_KEY
    return await axios
      .post(urlRegister, {
        email: credentials.email,
        password: credentials.password
      })
      .then((ressult) => {
        vuexContext.commit('SET_TOKEN', ressult.data.idToken)
        vuexContext.commit('SET_ISAUTHEN', true)
        this.$router.push('/auth/login')
      })
      .catch((error) => {
        vuexContext.commit('SET_TOKEN', null)
        vuexContext.commit('SET_ISAUTHEN', false)
      })
  },

  initAuth (vuexContext, req) {
    let token, tokenExpiration
    if (req) {
      // Haddle fist time go to Page
      // if not isset cooke -> return false
      if (!req.headers.cookie) { return false }
      // else isset cookie
      const tokenKey = req.headers.cookie
        .split(';')
        .find(c => c.trim().startsWith('token='))

      const tokenExpirationKey = req.headers.cookie
        .split(';')
        .find(c => c.trim().startsWith('tokenExpiration='))

      if (!tokenKey || !tokenExpirationKey) {
        vuexContext.dispatch('logout')
        return false
      }
      token = tokenKey.split('=')[1]
      tokenExpiration = tokenExpirationKey.split('=')[1]
    } else {
      token = localStorage.getItem('token')
      tokenExpiration = localStorage.getItem('tokenExpiration')
      if (new Date().getTime() > tokenExpiration || !token) {
        vuexContext.dispatch('logout')
        return false
      }
    }
    vuexContext.dispatch(
      'setLogoutTimer',
      tokenExpiration - new Date().getTime()
    )
    vuexContext.commit('SET_TOKEN', token)
    vuexContext.commit('SET_ISAUTHEN', true)
    const urlGetUser = process.env.API_URL_API + 'get-user'
    this.$axios
      .$get(urlGetUser)
      .then((data) => {
        vuexContext.dispatch('setUser', data.data)
      })
  }

}
