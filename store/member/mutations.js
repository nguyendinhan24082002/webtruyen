export default {
  SET_USER (state, payload) {
    state.loaderUser = payload
  },
  SET_ISAUTHEN (state, payload) {
    state.isAuthen = payload
  },
  SET_TOKEN (state, token) {
    state.loaderToken = token
  },
  GET_TOKEN (state, payload) {
    state.loaderToken = payload
  },
  CLEAR_TOKEN (state) {
    state.loaderToken = null
  },
  SET_CURRENT_USER (state, payload) {
    state.currentUser = payload
  },
  SET_AVATAR_USER (state, payload) {
    state.currentUser.user.avatar = payload
    this.$cookies.set('currentUser', state.currentUser)
  },
  RESET_STORE (state) {
    state.loaderUser = null,
    state.isAuthen = false,
    state.loaderToken = null,
    state.currentUser = null
  }
}
